import 'package:flutter/material.dart';
import 'picture.dart';
import 'package:gui_app/main.dart';
import 'package:camera/camera.dart';

class Kamera extends StatelessWidget {
  final snackBar = SnackBar(
    content: Text('Kamera konnte leider nicht initialisiert werden!'),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: minze,
        title: Text("Kamera"),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 10),
              // Button zum beginnen der Bildaufnahme
              child: ElevatedButton(
                style: style,
                onPressed: () async {
                  //Beim drücken des Buttons wird versucht die Kamera zu intialisieren
                  try {
                    WidgetsFlutterBinding.ensureInitialized();

                    // cameras bekommt eine Liste von Kameras
                    final cameras = await availableCameras();

                    // Je nach Index bekommt man eine andere Kamera
                    // [0] -> In der Regel Hauptkamera
                    // [1] -> In der Regel Frontkamera
                    final firstCamera = cameras[0];

                    //Neues Widget wird aufgerufen und bekommt die Kamera mitgegeben
                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) =>
                            TakePictureScreen(camera: firstCamera),
                      ),
                    );
                  } catch (_) {
                    // Falls der obere Schritt Fehlgeschlagen ist
                    // Verhindert den Absturz der App

                    print("Fehler beim initialisieren der Kamera.");
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  }
                },
                child: Text("Bild aufnehmen"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
