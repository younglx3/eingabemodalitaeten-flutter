import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gui_app/main.dart';

final _formKey = GlobalKey<FormState>();

class Tastatur extends StatelessWidget {
  final textController = TextEditingController();
  final emailController = TextEditingController();
  final nummerController = TextEditingController();
  final passwordController = TextEditingController();
  final freitextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tastatur"),
        backgroundColor: minze,
      ),
      //ScrollView, sodass es keine Probleme mit der Tastaur über dem Eingabefeld gibt
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(50),
          // Hier fängt das Formular an
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                //Normales Textfeld
                TextFormField(
                  // Standardtastatur, wird auch benutzt wenn der Parameter nicht mitübergeben wird
                  keyboardType: TextInputType.text,
                  autocorrect: false,
                  controller: textController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: gruen)),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: gruen,
                      ),
                    ),
                    labelText: 'Textfeld',
                    labelStyle: TextStyle(color: gruen),
                  ),
                ),
                SizedBox(height: 20),
                // Nummernfeld;
                TextFormField(
                  controller: nummerController,
                  // Der User kann nur Zahlen benutzen
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                  // Wenn wir Nummern bzw. Zahlen als Eingabe erwarten, sollte der User diese Tastatur bekommen
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: gruen,
                      ),
                    ),
                    labelText: 'Nummernfeld',
                    labelStyle: TextStyle(color: gruen),
                  ),
                ),
                SizedBox(height: 20),
                // E-Mailfeld
                TextFormField(
                  controller: emailController,
                  // Bei E-Mail-Eingaben diese Tastatur zur Verfügung stellen
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: gruen,
                      ),
                    ),
                    labelText: 'Emailfeld',
                    labelStyle: TextStyle(color: gruen),
                  ),
                ),
                SizedBox(height: 20),
                // Passwortfeld
                TextFormField(
                  controller: passwordController,
                  obscureText: true, // Es wird nur das letzte Zeichen angezeigt
                  enableSuggestions: false, // Vorschläge werden nicht angezeigt
                  autocorrect: false, // Autocorrect ausschalten
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: gruen,
                      ),
                    ),
                    labelText: 'Passwort',
                    labelStyle: TextStyle(color: gruen),
                  ),
                  // Validator zum prüfen, ob etwas eingegeben worden ist
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Bitte Passwort eingeben.";
                    }
                    return null;
                  },
                ),
                SizedBox(height: 20),
                //Freitextfeld, falls man längere Texte als Eingabe will
                TextFormField(
                  maxLines: 5, // maximale Anzahl an Zeilen
                  maxLength: 150, // maximale Anzahl an Zeichen
                  controller: freitextController,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: gruen,
                      ),
                    ),
                    labelStyle: TextStyle(color: gruen),
                    labelText: 'Freitext',
                  ),
                ),
                SizedBox(height: 40),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(primary: minze),
                      onPressed: () {
                        // reset() setzt alle Felder wieder auf den Initalwert zurück.
                        _formKey.currentState!.reset();
                      },
                      child: Text('Löschen'),
                    ),
                    SizedBox(width: 25),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(primary: minze),
                      onPressed: () {
                        // Wenn alle Validatoren der Felder des Formulars gültig sind.
                        if (_formKey.currentState!.validate()) {
                          // Hier können wir mit den geprüften Daten aus dem Formular etwas machen.
                          print(
                              "Formular ist gültig und kann verarbeitet werden");
                          print(textController.text);
                        } else {
                          print("Formular ist nicht gültig");
                        }
                      },
                      child: Text('Speichern'),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
