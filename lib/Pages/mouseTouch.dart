import 'package:flutter/material.dart';
import 'package:gui_app/main.dart';

/*Enum-Datentyp für die jeweiligen Zustände des Buttons*/
enum ButtonState { normal, pressed, longpressed, dragged }

/*Global einen style für Buttons festgelegt -> */
final ButtonStyle style = ElevatedButton.styleFrom(
  textStyle: TextStyle(
    color: Colors.white,
    fontSize: 20,
  ),
  minimumSize: Size(150.0, 100.0),
  primary: minze,
);

/*Stateful Widget, da sich die Elemente dynamisch verändern*/
class MouseTouch extends StatefulWidget {
  @override
  _MouseTouchState createState() => _MouseTouchState();
}

class _MouseTouchState extends State<MouseTouch> {
  /*Zustand des Buttons initiiert*/
  ButtonState buttonState = ButtonState.normal;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: minze,
        title: Text("Maus/Touch"),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 10),

              /*Button um Farbe eines anderen Buttons zu verändern; lässt sich lang oder kurz drücken
              Beim drücken wird der Zustand des ButtonStates geändert und der State neu geladen*/
              child: ElevatedButton(
                  style: style,
                  onPressed: () => setState(() {
                        buttonState = ButtonState.pressed;
                      }),
                  onLongPress: () => setState(() {
                        buttonState = ButtonState.longpressed;
                      }),
                  child: Text("Change Color")),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                //Hier wird das ziehbare Objekt implementiert
                Draggable<int>(
                  // Ziehbare Elemente können Daten mit sich tragen, die an das Ziel übergeben werden können
                  data: 0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      // Hier befindet sich das Draggable Widget(auch mehrere möglich);
                      // Entspricht dem Element, dass der User sieht bevor er es draggt
                      Container(
                        padding: EdgeInsets.all(5),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(
                            "Ziehbares Objekt",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        margin: EdgeInsets.only(top: 20),
                        height: 150.0,
                        width: 150.0,
                        color: minze,
                      ),
                    ],
                  ),
                  // Erfordliches Widget, muss mit angegeben;
                  // Entspicht dem Element das der User sieht, wenn er das Draggable zieht
                  feedback: Container(
                    padding: EdgeInsets.all(5),
                    margin: EdgeInsets.only(top: 20),
                    color: hellblau,
                    height: 150,
                    width: 150,
                  ),
                  // Entspricht dem Element, das zurück bleibt, wenn der User das Draggable zeiht
                  // Optional; wenn nicht mitangegeben, bleibt das urspüngliche Draggable Widget zurück
                  childWhenDragging: Container(
                    padding: EdgeInsets.all(5),
                    margin: EdgeInsets.only(top: 20),
                    height: 150.0,
                    width: 150.0,
                    color: gruen,
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        "Unter dem Objekt",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 30,
                ),
                // Enspricht dem Ziel an dem das draggable Element fallengelassen werden soll
                DragTarget<int>(
                  builder: (
                    BuildContext context,
                    List<dynamic> accepted,
                    List<dynamic> rejected,
                  ) {
                    return Container(
                      margin: EdgeInsets.only(top: 20),
                      height: 150.0,
                      width: 150.0,
                      color: tuerkis,
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          "Ziel für Objekt",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    );
                  },
                  // Hier wird eine Funktion übergeben, die ausgeführt wird, wenn das draggable-Element vom Ziel angenommen wird
                  onAccept: (int) {
                    setState(() {
                      buttonState = ButtonState.dragged;
                    });
                  },
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              child: RawMaterialButton(
                child: Icon(Icons.emoji_emotions_sharp,
                    color: Colors.yellow, size: 50),
                fillColor: getButtonColor(buttonState),
                shape: CircleBorder(),
                padding: EdgeInsets.all(30),
                onPressed: () {},
              ),
            ),
            SizedBox(
              height: 50,
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              child: ElevatedButton(
                  style: style,
                  onPressed: () => setState(() {
                        buttonState = ButtonState.normal;
                      }),
                  child: Text("Reset")),
            ),
          ],
        ),
      ),
    );
  }

  Color getButtonColor(ButtonState state) {
    if (state == ButtonState.normal) {
      return minze;
    } else if (state == ButtonState.pressed) {
      return tuerkis;
    } else if (state == ButtonState.longpressed) {
      return gruen;
    } else {
      return hellblau;
    }
  }
}
