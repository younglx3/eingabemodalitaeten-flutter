import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'dart:io';
import 'package:gui_app/main.dart';

class TakePictureScreen extends StatefulWidget {
  final CameraDescription camera;
  // Das Widget muss eine Kamera mitübergeben bekommen
  const TakePictureScreen({
    Key? key,
    required this.camera,
  }) : super(key: key);

  @override
  TakePictureScreenState createState() => TakePictureScreenState();
}

class TakePictureScreenState extends State<TakePictureScreen> {
  late CameraController _controller;
  late Future<void> _initializeControllerFuture;

  @override
  void initState() {
    super.initState();
    // CameraController-Objekt erzeugen
    // Parameter sind die Kamera und die gewünschte Auflösung
    _controller = CameraController(
      widget.camera,
      ResolutionPreset.medium,
    );

    // Controller wird initialisiert
    _initializeControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    // Wenn das Widget verworfen wird muss der Controller mitverworfen werden
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Bild aufnehmen'),
        backgroundColor: minze,
      ),

      // Controller muss initialisiert sein, bevor man den Output der Kamera bekommt
      // FutureBuilder wartet bis der Controller initialisiert worden ist
      body: FutureBuilder<void>(
        future: _initializeControllerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            // Falls der Controller initialisiert worden ist -> Camera Preview
            return CameraPreview(_controller);
          } else {
            // Ansonsten wird ein Ladecirkel angezeigt
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),

      //Button zum Aufnehmen eins Bildes
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          // Bild wird versucht aufzunehmen
          // Wichtig, um Fehler abzufangen und Absturz zu verhindern

          try {
            // Controller muss initialisiert sein
            await _initializeControllerFuture;

            // Der Path vom Bild wird in image gespeichert
            final image = await _controller.takePicture();

            // Bild wurde erfolgreich aufgenommen -> Aufgenommes wird dem User angezeigt
            // Dafür wird ein neues Widget aufgerufen, welches den imagePath bekommt
            await Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => DisplayPictureScreen(
                  imagePath: image.path,
                ),
              ),
            );
          } catch (e) {
            // Bei einem Fehler -> Output an die Konsole
            print(e);
          }
        },
        child: const Icon(
          Icons.camera_alt,
        ),
        backgroundColor: minze,
      ),
    );
  }
}

// Hier wird das aufgenommene Bild angezeigt
class DisplayPictureScreen extends StatelessWidget {
  final String imagePath;

  const DisplayPictureScreen({Key? key, required this.imagePath})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Dein Bild'),
        backgroundColor: minze,
      ),
      // Über Image.File und dem Path zum Bild kann das Bild angezeigt werden
      body: Image.file(
        File(imagePath),
      ),
    );
  }
}
