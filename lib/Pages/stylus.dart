import 'dart:ui';
import 'package:gui_app/main.dart';
import 'package:flutter/material.dart';

class Stylus extends StatefulWidget {
  @override
  _StylusState createState() => _StylusState();
}

class _StylusState extends State<Stylus> {
  // Variablen x, y für die Position
  double x = 0.0;
  double y = 0.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Stylus"),
        backgroundColor: minze,
      ),
      // Ruft bei Pointer Events eine Funktion auf
      // Gibt gleichzeitig nützliche Daten zum Event an die Funktion mit
      body: Listener(
        // Die Callback-Funktion, die aufgerufen wird
        onPointerDown: _updateData,
        child: Center(
          child: Container(
            padding: EdgeInsets.all(5),
            child: Column(
              children: [
                Text(
                  "Position beim klicken: (${x.toStringAsFixed(2)}, ${x.toStringAsFixed(2)})",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
            height: 300.0,
            width: 300.0,
            color: minze,
          ),
        ),
      ),
    );
  }

  // Die Callback-Funktion
  // Innerhalb des PointerEvent-Objekts befinden sich informationen wie z.B.
  // Position, Druckstärke, ...
  void _updateData(PointerEvent details) {
    setState(() {
      x = details.position.dx;
      y = details.position.dy;
    });
  }
}
