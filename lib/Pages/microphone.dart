import 'package:flutter/material.dart';
import 'package:gui_app/main.dart';
import 'package:just_audio/just_audio.dart';
import 'package:microphone/microphone.dart';

// Die einzelnen States des Microphones in einem enum Datentyp gespeichert
enum AudioState { start, recording, stop, play }

// StatefulWidget da wir mit States arbeiten und der Screen sich dynamisch anpassen soll
class Microphone extends StatefulWidget {
  @override
  _MicrophoneState createState() => _MicrophoneState();
}

class _MicrophoneState extends State<Microphone> {
  late MicrophoneRecorder _recorder;
  late AudioState audioState;
  AudioPlayer? _audioPlayer = AudioPlayer();

  @override
  void initState() {
    super.initState();
    // Beim initieren des Widgets wird auch ein MicrophoneRecorder-Objekt initiiert
    // Das Objekt muss später wieder verworfen werden
    _recorder = MicrophoneRecorder()..init();
    audioState = AudioState.start;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: minze,
          title: Text("Mikrofon"),
        ),
        body: Center(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              // Der Button zum starten & stoppen der Aufnahme
              AnimatedContainer(
                duration: Duration(milliseconds: 300),
                padding: EdgeInsets.all(24),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: handleAudioColour(),
                ),
                child: RawMaterialButton(
                  fillColor: Colors.white,
                  shape: CircleBorder(),
                  padding: EdgeInsets.all(30),

                  // Je nach State passiert etwas anderes s. Funktion
                  onPressed: () => handleAudioState(audioState),
                  // Je nach State ein anderes Icon auf dem Button
                  child: getIcon(audioState),
                ),
              ),
              SizedBox(width: 20),

              // Kommt nur zum Vorschein wenn der State übereinstimmt
              if (audioState == AudioState.play ||
                  audioState == AudioState.stop)
                Container(
                  padding: EdgeInsets.all(24),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: minze,
                  ),
                  // Button um neue Aufnahme zu beginnen
                  child: RawMaterialButton(
                    fillColor: Colors.white,
                    shape: CircleBorder(),
                    padding: EdgeInsets.all(30),

                    // Aufnahme verworfen & ein neues Objekt für neue Aufnahme initiiert
                    onPressed: () => setState(() {
                      audioState = AudioState.start;
                      _recorder.dispose();
                      _recorder = MicrophoneRecorder()..init();
                    }),
                    child: Icon(Icons.replay, size: 50),
                  ),
                ),
            ],
          ),
        ));
  }

  void handleAudioState(AudioState state) {
    setState(() {
      if (audioState == AudioState.start) {
        // Aufnahme beginnt -> State wird auf recording gesetzt
        audioState = AudioState.recording;
        _recorder.start();
      } else if (audioState == AudioState.recording) {
        //Aufnahme wurde gestoppt -> Aufnahme beenden & State auf abspielen setzen
        audioState = AudioState.play;
        _recorder.stop();
      } else if (audioState == AudioState.play) {
        // Abspielen der Aufnahme -> State auf stoppen setzen
        audioState = AudioState.stop;
        // AudioPlayer-Objekt wird benötigt zum abspielen der Aufnahme
        _audioPlayer = AudioPlayer();
        // AudioPlayer bekommt die URL der Aufnahme und spielt diese ab
        _audioPlayer!.setUrl(_recorder.value.recording!.url).then(
          (_) {
            return _audioPlayer!.play().then((_) {
              setState(() => audioState = AudioState.play);
            });
          },
        );
        // Das Abspielen wird gestoppt & State wieder auf play gesetzt
      } else if (audioState == AudioState.stop) {
        audioState = AudioState.play;
        _audioPlayer!.stop();
      }
    });
  }

  // Je nach State bekommt der Button ein anderes Icon
  Icon getIcon(AudioState state) {
    switch (state) {
      case AudioState.play:
        return Icon(Icons.play_arrow, size: 50);
      case AudioState.stop:
        return Icon(Icons.stop, size: 50);
      case AudioState.recording:
        return Icon(Icons.pause, color: Colors.redAccent, size: 50);
      default:
        return Icon(Icons.mic, size: 50);
    }
  }

  // Gilt auch für die Farbe
  Color handleAudioColour() {
    if (audioState == AudioState.recording) {
      return Colors.deepOrangeAccent.shade700.withOpacity(0.5);
    } else if (audioState == AudioState.stop) {
      return Colors.green.shade900;
    } else {
      return minze;
    }
  }
}
