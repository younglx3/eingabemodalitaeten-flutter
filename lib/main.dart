import 'package:flutter/material.dart';
import 'package:gui_app/Pages/microphone.dart';
import 'Pages/mouseTouch.dart';
import 'Pages/keyboard.dart';
import 'Pages/camera.dart';
import 'Pages/stylus.dart';

const hellblau = Color(0xffD1EDE1);
const minze = Color(0xff7BC5AE);
const tuerkis = Color(0xff028C6A);
const gruen = Color(0xff003E19);

final ButtonStyle style = ElevatedButton.styleFrom(
  textStyle: TextStyle(
    color: Colors.white,
    fontSize: 20,
  ),
  minimumSize: Size(150.0, 100.0),
  primary: minze,
);

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: _MyAppState(),
    );
  }
}

class _MyAppState extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: minze,
        title: Text("GUI App"),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 10),
                child: ElevatedButton(
                    style: style,
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MouseTouch()));
                    },
                    child: Text("Maus/Touch")),
              ),
              Container(
                margin: EdgeInsets.only(top: 10),
                child: ElevatedButton(
                    style: style,
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Tastatur()));
                    },
                    child: Text("Tastatur")),
              ),
              Container(
                margin: EdgeInsets.only(top: 10),
                child: ElevatedButton(
                    style: style,
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => Microphone()));
                    },
                    child: Text("Mikrofon")),
              ),
              Container(
                margin: EdgeInsets.only(top: 10),
                child: ElevatedButton(
                    style: style,
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Kamera()));
                    },
                    child: Text("Kamera")),
              ),
              Container(
                margin: EdgeInsets.only(top: 10),
                child: ElevatedButton(
                    style: style,
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Stylus()));
                    },
                    child: Text("Stylus")),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
