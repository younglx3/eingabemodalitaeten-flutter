## Allgemeines zur GUI App

Eine App für den GUI-Kurs, die zeigen soll, wie man auf unterschiedliche Art und Weise Eingaben vom Benutzer entgegennehmen kann und wie es implementiert werden kann. Die App ist in Dart programmiert. Bei Fragen könnt ihr euch gerne an mich wenden. 

## Getting Started

Repository clonen und mit VS Code oder Android Studio über Web oder einem Emulator ausführen.

Flutter und Dart sind erforderlich: https://flutter.dev/docs/get-started/install 
